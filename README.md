# Edmodo Interview Homework

This repository contains a bare-bone application including basic authentication.

# To Run the application

Make sure you have Ruby (2.2 or above) and Bundler installed.

To set up the database, run

```console
bundle install
rake db:reset
```

This will set up the database and it also create 3 users: `'teacher'`, `'student'`, and `'teju'`. It will create homework and assign homework to `'teju'` and `'student'`.


To start the web server, run

```
bundle exec rails server
```

# To test the application
Open your web browser and navigate to to [http://localhost:3010](http://localhost:3010).

You can log in as a teacher by using `'teacher'` as the username and log in as a student by using `'student'` or `'teju'` as the username.

Log in as a student (username `'student'` or `'teju'`) and answer some assigned questions.

Then, log in as 'teacher' to view submitted answers.

Thanks.