require 'rails_helper'

RSpec.describe "homework_submissions/edit", type: :view do
  before(:each) do
    @homework_submission = assign(:homework_submission, HomeworkSubmission.create!())
  end

  it "renders the edit homework_submission form" do
    render

    assert_select "form[action=?][method=?]", homework_submission_path(@homework_submission), "post" do
    end
  end
end
