require 'rails_helper'

RSpec.describe "homework_submissions/new", type: :view do
  before(:each) do
    assign(:homework_submission, HomeworkSubmission.new())
  end

  it "renders new homework_submission form" do
    render

    assert_select "form[action=?][method=?]", homework_submissions_path, "post" do
    end
  end
end
