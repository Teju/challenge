require 'rails_helper'

RSpec.describe "homework_assignments/edit", type: :view do
  before(:each) do
    @homework_assignment = assign(:homework_assignment, HomeworkAssignment.create!())
  end

  it "renders the edit homework_assignment form" do
    render

    assert_select "form[action=?][method=?]", homework_assignment_path(@homework_assignment), "post" do
    end
  end
end
