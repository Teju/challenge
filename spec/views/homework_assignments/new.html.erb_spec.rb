require 'rails_helper'

RSpec.describe "homework_assignments/new", type: :view do
  before(:each) do
    assign(:homework_assignment, HomeworkAssignment.new())
  end

  it "renders new homework_assignment form" do
    render

    assert_select "form[action=?][method=?]", homework_assignments_path, "post" do
    end
  end
end
