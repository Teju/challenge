require "rails_helper"

RSpec.describe HomeworkSubmissionsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/homework_submissions").to route_to("homework_submissions#index")
    end

    it "routes to #new" do
      expect(:get => "/homework_submissions/new").to route_to("homework_submissions#new")
    end

    it "routes to #show" do
      expect(:get => "/homework_submissions/1").to route_to("homework_submissions#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/homework_submissions/1/edit").to route_to("homework_submissions#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/homework_submissions").to route_to("homework_submissions#create")
    end

    it "routes to #update" do
      expect(:put => "/homework_submissions/1").to route_to("homework_submissions#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/homework_submissions/1").to route_to("homework_submissions#destroy", :id => "1")
    end

  end
end
