require "rails_helper"

RSpec.describe HomeworkAssignmentsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/homework_assignments").to route_to("homework_assignments#index")
    end

    it "routes to #new" do
      expect(:get => "/homework_assignments/new").to route_to("homework_assignments#new")
    end

    it "routes to #show" do
      expect(:get => "/homework_assignments/1").to route_to("homework_assignments#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/homework_assignments/1/edit").to route_to("homework_assignments#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/homework_assignments").to route_to("homework_assignments#create")
    end

    it "routes to #update" do
      expect(:put => "/homework_assignments/1").to route_to("homework_assignments#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/homework_assignments/1").to route_to("homework_assignments#destroy", :id => "1")
    end

  end
end
