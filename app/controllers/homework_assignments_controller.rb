class HomeworkAssignmentsController < ApplicationController
  before_action :set_homework_assignment, only: [:show, :edit, :update, :destroy]

  # GET /homework_assignments
  # GET /homework_assignments.json
  def index
    @homework_assignments = HomeworkAssignment.all
  end

  # GET /homework_assignments/1
  # GET /homework_assignments/1.json
  def show
  end

  # GET /homework_assignments/new
  def new
    @homework_assignment = HomeworkAssignment.new
  end

  # GET /homework_assignments/1/edit
  def edit
  end

  # POST /homework_assignments
  # POST /homework_assignments.json
  def create
    @homework_assignment = HomeworkAssignment.new(homework_assignment_params)

    respond_to do |format|
      if @homework_assignment.save
        format.html { redirect_to @homework_assignment, notice: 'Homework assignment was successfully created.' }
        format.json { render :show, status: :created, location: @homework_assignment }
      else
        format.html { render :new }
        format.json { render json: @homework_assignment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /homework_assignments/1
  # PATCH/PUT /homework_assignments/1.json
  def update
    respond_to do |format|
      if @homework_assignment.update(homework_assignment_params)
        format.html { redirect_to @homework_assignment, notice: 'Homework assignment was successfully updated.' }
        format.json { render :show, status: :ok, location: @homework_assignment }
      else
        format.html { render :edit }
        format.json { render json: @homework_assignment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /homework_assignments/1
  # DELETE /homework_assignments/1.json
  def destroy
    @homework_assignment.destroy
    respond_to do |format|
      format.html { redirect_to homework_assignments_url, notice: 'Homework assignment was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_homework_assignment
      @homework_assignment = HomeworkAssignment.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def homework_assignment_params
      params[:homework_assignment]
    end
end
