class HomeworksController < ApplicationController
  before_action :set_homework, only: [:show, :edit, :update, :destroy]

  # GET /homeworks
  # GET /homeworks.json
  def index
    if (session[:user_role] == 'teacher')
       @homeworks = Homework.all()
    else
      @homeworks = Homework.joins(:homework_assignments)
                .where("homework_assignments.user_id" => session[:user_id])
    end
           

    if (@homeworks.blank?) 
      flash[:notice] = "Nothing to show...please check back later."
    end

  
  end



 
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_homework
      @homework = Homework.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def homework_params
      params[:homework]
    end
end
