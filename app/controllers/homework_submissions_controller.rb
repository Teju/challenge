class HomeworkSubmissionsController < ApplicationController
  before_action :set_homework_submission, only: [:show, :edit, :update, :destroy]

  # GET /homework_submissions
  # GET /homework_submissions.json
  def index
    @homework_submissions = HomeworkSubmission.find(params[:id])
  end


  # GET /homework_submissions/new
  def new
    @homework_id = params[:id]
    @homework_assignment_id = HomeworkAssignment.where("homework_id" => @homework_id).where("user_id"=>session[:user_id]).first.id
    @question = Homework.find(@homework_id).question
    @homework_submission = HomeworkSubmission.new
  end

  # GET /homework_submissions/1/edit
  def edit
  end

  # POST /homework_submissions
  # POST /homework_submissions.json
  def create
    homework_submission  = homework_submission_params
    homework_submission[:homework_submission][:user_id] = session[:user_id]

    @homework_submission = HomeworkSubmission.new(homework_submission_params)

    respond_to do |format|
      if @homework_submission.save
        format.html { redirect_to '/', notice: 'Homework submission was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end

  # GET /homework_submissions
  # GET /homework_submissions.json
  def submissionsByUserForHomework
    homework_id = params[:homework_id]
    user_id = params[:user_id]

    @user = User.find(user_id)
    @homework = Homework.find(homework_id)
    # TODO - Better way to parametrize and avoid injection hole?
    @submissions = HomeworkSubmission.includes(:homework_assignment).joins("INNER JOIN homework_assignments ON homework_assignments.id = homework_submissions.homework_assignment_id AND homework_assignments.homework_id = " + HomeworkSubmission.sanitize(homework_id) + " AND homework_assignments.user_id = " + HomeworkSubmission.sanitize(user_id) + " ORDER BY homework_submissions.created_at DESC ")
  
    if (@submissions.blank?) 
      flash[:notice] = "Nothing to show...please check back later."
    end

  end

  def viewLatestForAssignment
    homework_id = params[:homework_id]
    @homework = Homework.find(homework_id)

    # TODO - injection hole - how do I parametrize this?
    @submissions = HomeworkSubmission.select("MAX(homework_submissions.created_at) as max_created_at, *").joins("INNER JOIN homework_assignments ON homework_assignments.id = homework_submissions.homework_assignment_id AND homework_assignments.homework_id = ", HomeworkSubmission.sanitize(homework_id),  "GROUP BY homework_assignment_id").includes(:homework_assignment =>  [:user])

    if (@submissions.blank?) 
      flash[:notice] = "Nothing to show...please check back later."
    end

  end

 
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_homework_submission
      @homework_submission = HomeworkSubmission.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def homework_submission_params
      params.require(:homework_submission).permit(:answer, :homework_assignment_id)
    end
end
