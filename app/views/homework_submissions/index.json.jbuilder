json.array!(@homework_submissions) do |homework_submission|
  json.extract! homework_submission, :id
  json.url homework_submission_url(homework_submission, format: :json)
end
