json.array!(@homework_assignments) do |homework_assignment|
  json.extract! homework_assignment, :id
  json.url homework_assignment_url(homework_assignment, format: :json)
end
