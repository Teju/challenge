class Homework < ActiveRecord::Base
  validates :title, presence: true
  validates :question, presence: true
  validates :due_by, presence: true

  has_many :homework_assignments
	has_many :users, through: :homework_assignments

end
