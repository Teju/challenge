class User < ActiveRecord::Base
  enum role: [:teacher, :student]

  validates :username, presence: true, uniqueness: true
  validates :role, presence: true

  has_many :homework_assignments
	has_many :homeworks, through: :homework_assignments

end
