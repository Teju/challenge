class HomeworkAssignment < ActiveRecord::Base
	belongs_to :user

	has_many :homework_submissions
	belongs_to :homework
end
