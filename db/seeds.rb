# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
u1 = FactoryGirl.create(:teacher, username: 'teacher')
u2 = FactoryGirl.create(:student, username: 'student')
u3 = FactoryGirl.create(:student, username: 'teju')


h1 = Homework.create(:title => 'Trivia', :question => "What is the name of the astronaut who left a twin behind?", :due_by => "2016-12-12")
h2 = Homework.create(:title => 'Nature', :question => "Which mammal lays eggs?", :due_by => "2016-11-12")
h3 = Homework.create(:title => 'Current Affairs', :question => "Hillary or Donald?", :due_by => "2016-11-17")


h1.homework_assignments.create(:user => u2, :homework => h1)
h2.homework_assignments.create(:user => u2, :homework => h2)
h3.homework_assignments.create(:user => u2, :homework => h3)
h3.homework_assignments.create(:user => u3, :homework => h3)
