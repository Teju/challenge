class CreateHomeworks < ActiveRecord::Migration
  def change
    create_table :homeworks do |t|
      t.string :title, null: false
      t.string :question, null: false
      t.datetime :due_by, null: false

      t.timestamps
    end
  end
end
