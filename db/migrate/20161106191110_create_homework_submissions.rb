class CreateHomeworkSubmissions < ActiveRecord::Migration
  def change
    create_table :homework_submissions do |t|
			t.belongs_to :homework_assignment, index: true
			t.text :answer

      t.timestamps null: false
    end
  end
end
