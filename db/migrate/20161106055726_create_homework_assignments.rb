class CreateHomeworkAssignments < ActiveRecord::Migration
  def change
    create_table :homework_assignments do |t|
			t.belongs_to :homework, index: true
			t.belongs_to :user, index: true

      t.timestamps 
    end
  end
end
